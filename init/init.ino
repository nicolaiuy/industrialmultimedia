#include <ArduinoJson.h>
#include "config.hpp"
#include "_device.hpp"
#include "_server.hpp"
#include "_service.hpp"

void setup()
{
  // Initialize Data
  if (!SPIFFS.begin())
  {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }
  setupServer();
}

void loop()
{
  loopServer();
  // getAllUsers();
}
