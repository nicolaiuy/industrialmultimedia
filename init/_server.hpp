#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <ESPAsyncWebServer.h>
#include <ESPAsyncTCP.h>
#include <Hash.h>
#include <FS.h>

WiFiClient wifiClient;
AsyncWebServer server(80);

const char *path = "/data.json";
boolean hasWiFiCredentials;

String getCredentials()
{
    File file = SPIFFS.open(path, "r");
    if (!file || file.isDirectory())
    {
        Serial.println("- empty file or failed to open file");
        return "";
    }

    String fileContent;
    while (file.available())
    {
        fileContent += String((char)file.read());
    }

    const size_t bufferSize = JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(5) + JSON_OBJECT_SIZE(8) + 370;
    DynamicJsonBuffer jsonBuffer(bufferSize);
    JsonObject &json = jsonBuffer.parseObject(fileContent);

    String ssid = json["ssid"];
    String password = json["password"];
    String response = "{\"ssid\":" + ssid + ", \"password\":" + password + "}";

    return response;
}

void printCreds()
{
    String tmp = getCredentials();
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject &credentials = jsonBuffer.parseObject(tmp);
    String ssid = credentials["ssid"];
    String password = credentials["password"];
    Serial.println(ssid + " " + password);
}

void setCredentials(String ssid, String password)
{
    File file = SPIFFS.open(path, "w");
    if (!file)
    {
        Serial.println("- failed to open file for writing");
        return;
    }

    String tmp = getCredentials();
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject &credentials = jsonBuffer.parseObject(tmp);
    credentials["ssid"] = ssid;
    credentials["password"] = password;

    String jsonStr;
    credentials.printTo(jsonStr);

    if (file.print(jsonStr))
    {
        Serial.println("- file written");
    }
    else
    {
        Serial.println("- write failed");
    }
    file.close();
    Serial.println("New creds: ");
    printCreds();
}

boolean hasCredentials()
{
    String tmp = getCredentials();
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject &credentials = jsonBuffer.parseObject(tmp);
    String ssid = credentials["ssid"];
    String password = credentials["password"];

    return ssid.length() > 0 && password.length() > 0;
}

void notFound(AsyncWebServerRequest *request)
{
    request->send(404, "text/plain", "Not found");
}

void initRoutes()
{
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send(SPIFFS, "/index.html", String()); });

    server.on("/", HTTP_POST, [](AsyncWebServerRequest *request)
              {
                  String ssid = request->arg("ssid");
                  String pwd = request->arg("pwd");

                  boolean validCredentials = false;

                  if (validCredentials)
                  {
                      setCredentials(ssid, pwd);
                      request->send(SPIFFS, "/index.html", String());
                      return;
                  }

                  AsyncWebServerResponse *response = request->beginResponse(200, "text/javascript", "false");

                  request->send(response);
              });

    server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send(SPIFFS, "/style.css", "text/css"); });

    server.on("/jquery.min.js", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send(SPIFFS, "/jquery.min.js", "text/javascript"); });

    server.on("/script.js", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send(SPIFFS, "/script.js", "text/javascript"); });

    server.onNotFound([](AsyncWebServerRequest *request)
                      { request->send(404, "text/plain", "Not found"); });
}

void setupLAN()
{

    WiFi.softAP(local_ssid, local_password);
    IPAddress IP = WiFi.softAPIP();
    server.begin();
}

void setupWLAN()
{
    String tmp = getCredentials();
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject &credentials = jsonBuffer.parseObject(tmp);
    String ssid = credentials["ssid"];
    String password = credentials["password"];

    // Intialize WiFi Connection
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);

    if (WiFi.waitForConnectResult() != WL_CONNECTED)
    {
        Serial.println("WiFi Failed!");
        return;
    }
    Serial.println(WiFi.localIP());
}

void setupServer()
{
    Serial.begin(115200);

    initRoutes();
    setupLAN();

    printCreds();
}

// States: Active, Inactive
boolean state = false;

void loopServer()
{
    // hasWiFiCredentials = hasCredentials();
    hasWiFiCredentials = true;

    if (hasWiFiCredentials && !state)
    {
        state = true;
        setupWLAN();
    }
    else if (!hasWiFiCredentials)
    {
        state = false;
        setupLAN();
    }
}